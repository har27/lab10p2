/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package calculatorwithocprincipal;

/**
 *
 * @author hptop
 */
public class Multiply extends Calculator {
    
    public Multiply( double operand1 , double operand2){
        super( operand1 , operand2 );
    }
    public double multiply( ){
        return operand1 * operand2;
    }
}
