/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package calculatorwithocprincipal;

/**
 *
 * @author hptop
 */
public abstract class Calculator {
    private double operand1;
    private double operand2;
    
    public Calculator (double operand1 , double operand2 ){
        super(operand1 ,operand2);
    }
    
    public double add(){
        return operand1 + operand2;
    }
    
}
